require("telescope").setup({
	mappings = {
		i = {
			["<C-Down>"] = require("telescope.actions").cycle_history_next,
			["<C-Up>"] = require("telescope.actions").cycle_history_prev,
		},
	},
	extensions = {
		fzf = {
			fuzzy = true,
			override_generic_sorter = true, -- override the generic sorter
			override_file_sorter = true, -- override the file sorter
            case_mode = "respect_case", -- or "ignore_case" or "respect_case"
		},
	},
})

require("telescope").load_extension("fzf")

require('mystuff/utils')
local exports = {}

function exports.reload()
    -- Telescope will give us something like ju/colors.lua,
    -- so this function convert the selected entry to
    local function get_module_name(s)
        local module_name;

        module_name = s:gsub("%.lua", "")
        module_name = module_name:gsub("%/", ".")
        module_name = module_name:gsub("%.init", "")

        return module_name
    end

    local prompt_title = "~ neovim modules ~"

    -- sets the path to the lua folder
    local path = "~/.config/nvim/lua/mystuff"

    local opts = {
        prompt_title = prompt_title,
        cwd = path,

        attach_mappings = function(_, map)
            -- Adds a new map to ctrl+e.
            map("i", "<c-e>", function(_)
                -- these two a very self-explanatory
                local entry =
                    require("telescope.actions.state").get_selected_entry()
                local name = get_module_name(entry.value)

                -- call the helper method to reload the module
                -- and give some feedback
                R(name)
            end)

        end
    }

    -- call the builtin method to list files
    require('telescope.builtin').find_files(opts)
end

function exports.cooler()
    -- Telescope will give us something like ju/colors.lua,
    -- so this function convert the selected entry to
    local prompt_title = "Cool Files"

    -- sets the path to the lua folder
    local path = "."

    local opts = {
        prompt_title = prompt_title,
        cwd = path,

        attach_mappings = function(_, map)
            map("n", "D", function(_)
                local entry =
                    require("telescope.actions.state").get_selected_entry()
                AsyncRun("dragon-drag-and-drop", entry.value)

            end)
            map("i", "<c-o>", function(_)
                local entry =
                    require("telescope.actions.state").get_selected_entry()
                AsyncRun("xdg-open", entry.value)

            end)
            map("i", "<C-Up>", require("telescope.actions").cycle_history_prev)
            map("i", "<C-Down>", require("telescope.actions").cycle_history_next)
            return true
        end
    }

    require('telescope.builtin').find_files(opts)
end


return exports;
